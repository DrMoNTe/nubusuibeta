import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  logo = 'assets/icon/bus_icon.png'
  active = false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }


  closeFirst() {
    this.menu.enable(false, 'first');
    this.menu.close('first');
  }
  gotoHome(){
    this.menu.close('first');
  }
  gotoPath(){
    this.menu.close('first');
  }
  gotoAbout(){
    if(this.active){
      this.active  = false;
    }
    else{
      this.active  = true;
    }
    
  }
  getChangeClass() {
    if (this.active) {
      
      return 'about-active';
    }
    else {
      return 'about';
    }
  }
  getChangeAboutClass() {
    if (this.active) {
      
      return 'about-button-active';
    }
    else {
      return 'about-button';
    }
  }



  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
