import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PathPage } from './path.page';

const routes: Routes = [
  {
    path: '',
    component: PathPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PathPageRoutingModule {}
