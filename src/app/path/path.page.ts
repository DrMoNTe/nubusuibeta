import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ApiService } from '../services/api.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Plugins } from '@capacitor/core';
const { Geolocation } = Plugins;

declare var google;

@Component({
  selector: 'app-path',
  templateUrl: './path.page.html',
  styleUrls: ['./path.page.scss'],
})
export class PathPage {

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  markers = [];
  stop_markers = [];
  lines = [];
  stop: any;



  timeofStop: any;
  pathLine = [];
  loading: any;
  error: string;
  listofRedPath = [];
  listofYellowPath = [];
  historicalOverlay;

  constructor(public http: HttpClient, public apiService: ApiService) { }

  ionViewWillEnter() {
    console.log('WillEnter')
    this.loadMap();
  }
  ionViewDidEnter() {
    this.getData()

  }

  customPopoverOptions: any = {
    header: 'ป้ายรถไฟฟ้าโดยสาร',
    subHeader: 'เลือกป้ายเพื่อดูตำแหน่งพิกัด',
    //message: 'Only select your dominant hair color'
  };

  onSelectChange(e){
    let latLng = new google.maps.LatLng(e.target.value.wp_lat, e.target.value.wp_lng);
    console.log("E",e.target.value)
    this.map.setZoom(18),
    this.map.panTo(latLng);
    this.timeofStop = e.target.value;
    console.log('inTime', this.timeofStop)
  }
  async loadMap() {


    let latLng = new google.maps.LatLng(16.7426885, 100.1925963);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true

    };


    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    var ctaLayer = new google.maps.KmlLayer({
      url: 'assets/path.kml',
      map: this.map
    });

    this.apiService.getBus_stop().subscribe(
      res => {
        console.log('RES', res.data);
        this.stop = res.data;

        for (let loc of this.stop) {

          let latLng = new google.maps.LatLng(loc.wp_lat, loc.wp_lng);
          let image = {
            url: 'assets/sign.png',
            // This marker is 20 pixels wide by 32 pixels high.
            scaledSize: new google.maps.Size(35, 35),
            //origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 36)
          };
          let stop_marker = new google.maps.Marker({
            icon: image,
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            title: loc.name,
            opacity: 0.9
          

          });
          let info = new google.maps.InfoWindow({
            content: '<br>ป้าย : ' + loc.name + ' | สาย : '+loc.path +'</br>'+
                      loc.decs+'</p>'
          })
          stop_marker.addListener('click', () => {
            //this.stop_markers.map(stop_marker => stop_marker.setMap(null))
            this.map.setZoom(18),
            this.map.panTo(stop_marker.getPosition());
            info.open(this.map, stop_marker);
            this.timeofStop = loc;
            console.log('inTime', this.timeofStop)
          });
          info.addListener('closeclick', () => {
            this.timeofStop = null;
            console.log('CLICK')
            console.log('inTime', this.timeofStop)
          })
          this.stop_markers.push(stop_marker);


        }
      },
      err => {

        this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
      }
    );



  }

  getData() {
    this.http.get('assets/path.geojson').subscribe(res => {

      let data = res['features'];
      let redPath = data[0].geometry.coordinates;
      for (let path of redPath) {
        let temp = { "lat": path[1], "lng": path[0] };
        this.listofRedPath.push(temp);
      }
      let yellowPath = data[1].geometry.coordinates;
      for (let path of yellowPath) {
        let temp = { "lat": path[1], "lng": path[0] };
        this.listofYellowPath.push(temp);
      }
    },
      (err) => {
        alert('failed loading json data');
      });
  }
  toggleOverlay(path) {
    let srcImage;
    let imageBounds = {
      north: 16.752252,
      south: 16.736286,
      east: 100.204607,
      west: 100.184093
    };
    if (path == "RED") {
      srcImage = 'assets/RED.png';
    }
    else if (path == "YELLOW") {
      srcImage = 'assets/YELLOW.png';
    }
    if (this.historicalOverlay) {
      this.historicalOverlay.setMap(null);
    }

    this.historicalOverlay = new google.maps.GroundOverlay(
      srcImage,
      imageBounds
    );
    this.historicalOverlay.setMap(this.map);

  }

  createRedPath() {

    this.pathLine.map(line => line.setMap(null));
    let line = new google.maps.Polyline({
      path: this.listofRedPath,
      geodesic: true,
      strokeColor: '#FF3300',
      strokeOpacity: 1.0,
      strokeWeight: 5.0,
      map: this.map
    });
    this.pathLine.push(line);
    this.toggleOverlay("RED");
  }
  createYellowPath() {

    this.pathLine.map(line => line.setMap(null));
    let line = new google.maps.Polyline({
      path: this.listofYellowPath,
      geodesic: true,
      strokeColor: '#FFCC00',
      strokeOpacity: 1.0,
      strokeWeight: 5.0,
      map: this.map
    });
    this.pathLine.push(line);
    this.toggleOverlay("YELLOW");
  }
}
