import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PathPageRoutingModule } from './path-routing.module';

import { PathPage } from './path.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PathPageRoutingModule
  ],
  declarations: [PathPage]
})
export class PathPageModule {}
