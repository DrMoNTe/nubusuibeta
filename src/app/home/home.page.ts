import { ToastController, IonCard, AlertController, Platform, LoadingController } from '@ionic/angular';
import { Socket } from 'ngx-socket-io';

import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Plugins } from '@capacitor/core';
import { Observable } from 'rxjs';
const { Geolocation } = Plugins;

declare var google;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  locations: Observable<any>;

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  markers = [];
  stop_markers = [];
  lines = [];
  userLine = [];
  userMarker = [];
  historicalOverlay;

  stop: any;
  nearest: any;


  timeofStop: any;
  stream_data = '';
  stream_datas = [];
  cur = '';
  flow_data: any;
  tracked = true;
  following = "";
  userTracked = false;
  userPosition: any;
  data: any;
  error: string;
  pathLine=[];
  loading: any;

  listofRedPath = [];
  listofYellowPath = [];
  @ViewChild(IonCard, { read: ElementRef, static: true }) contentArea: ElementRef;
  @ViewChild("triggerElement", { read: ElementRef, static: true }) triggerElement: ElementRef;
  private observer: IntersectionObserver;

  constructor(public http: HttpClient,public apiService: ApiService, public loadingCtrl: LoadingController, private plt: Platform, public toastController: ToastController, private socket: Socket, private renderer: Renderer2, public alertController: AlertController) {

  }

  ionViewWillEnter() {
    this.loadMap().then(() => {
      this.getData();
      this.receiveData(this.tracked);



    });



    console.log(this.contentArea);
    this.observer = new IntersectionObserver(entries => {
      entries.forEach((entry: any) => {
        if (!entry.isIntersecting) {
          console.log("add tranform");
          this.renderer.addClass(this.contentArea.nativeElement, 'fullSize');
        }
        else {
          console.log("remove tranform");
          this.renderer.removeClass(this.contentArea.nativeElement, 'fullSize')
        }
      });
    });
    this.observer.observe(this.triggerElement.nativeElement);

  }

  getData() {
    this.http.get('assets/path.geojson').subscribe(res => {
          
          let data = res['features'];
          let redPath = data[0].geometry.coordinates;
          for (let path of redPath){
            let temp = {"lat":path[1],"lng":path[0]};
            this.listofRedPath.push(temp);
          }
          let yellowPath = data[1].geometry.coordinates;
          for (let path of yellowPath){
            let temp = {"lat":path[1],"lng":path[0]};
            this.listofYellowPath.push(temp);
          }
        },
        (err) => {
          alert('failed loading json data');
        });
  }
  createRedPath(){
    this.pathLine.map(aline => aline.setMap(null));
    let aline = new google.maps.Polyline({
      path: this.listofRedPath,
      geodesic: true,
      strokeColor: '#FF3300',
      strokeOpacity: 0.3,
      strokeWeight: 5.0,
      map: this.map
    });
    this.pathLine.push(aline);
  }
  createYellowPath(){
    this.pathLine.map(aline => aline.setMap(null));
    let aline = new google.maps.Polyline({
      path: this.listofYellowPath,
      geodesic: true,
      strokeColor: '#FFCC00',
      strokeOpacity: 0.3,
      strokeWeight: 5.0,
      map: this.map
    });
    this.pathLine.push(aline);
  }

  async getCurrentPosition() {
    const coordinates = await Geolocation.getCurrentPosition();
    console.log('Current', coordinates);
    return coordinates

  }

  async watchPosition() {
    const wait = Geolocation.watchPosition({}, (position, err) => {
      console.log('WATCH', position)
    })
    console.log('Wait', wait);
    return wait
  }

  clearStop(){
    this.timeofStop = null;
    let latLng = new google.maps.LatLng(16.7466885, 100.1945963);
    this.map.panTo(latLng);
    this.map.setZoom(15);
  }
  async findNearMe() {
    this.loading = await this.loadingCtrl.create({
      message: 'Loading...'
    });
    await this.loading.present();
    console.log("TRCK",this.userTracked)
    if (!this.userTracked) {
      this.userTracked = true;
      Geolocation.watchPosition({}, (data, err) => {
        this.userPosition = data;
        console.log('USERPOS',this.userPosition)


        let image = {
          url: 'assets/man.png',
          // This marker is 20 pixels wide by 32 pixels high.
          scaledSize: new google.maps.Size(40, 40),
          //origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(20, 42)
        };
        console.log('curPos', data);

        let latLng = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
        this.apiService.findNearestBus_stop(JSON.stringify({ 'lat': data.coords.latitude, 'lng': data.coords.longitude })).subscribe(
          res => {
            this.userMarker.map(marker => marker.setMap(null));
            this.userLine.map(line => line.setMap(null));
    
            let temp = res.val;
            res.val = parseInt(temp);
            this.nearest = res;
            console.log('new res', res);
            this.timeofStop = res;

            let stop_latLng = new google.maps.LatLng(res.wp_lat, res.wp_lng);
            let line = new google.maps.Polyline({
              path: [stop_latLng, latLng],
              geodesic: true,
              strokeColor: '#FF9900',
              strokeOpacity: 1.0,
              strokeWeight: 5.0,
              map: this.map
            });
            this.userLine.push(line);


            let marker = new google.maps.Marker({
              position: latLng,
              icon: image,
              map: this.map
            });
            this.userMarker.push(marker);

            this.map.setZoom(17);
            this.map.panTo(latLng);


          },
          err => {
            // Set the error information to display in the template
            this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
          }
        );


      });


    }
    else {
      this.userTracked = false;
      this.nearest = null;
      this.userMarker.map(marker => marker.setMap(null));
      this.userLine.map(line => line.setMap(null));
      let latLng = new google.maps.LatLng(16.7466885, 100.1945963);
      this.map.panTo(latLng);
      this.map.setZoom(15);
    }
    this.loading.dismiss();

  }

  /*   async findNearMe() {
      this.loading = await this.loadingCtrl.create({
        message: 'Loading...'
      });
      await this.loading.present();
  
      if (!this.userTracked) {
        this.userTracked = true;
        this.getCurrentPosition().then(data => {
          this.userPosition = data;
  
          let image = {
            url: 'assets/man.png',
            // This marker is 20 pixels wide by 32 pixels high.
            scaledSize: new google.maps.Size(40, 40),
            //origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(20, 42)
          };
          console.log('curPos', data);
  
          let latLng = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
          this.apiService.findNearestBus_stop(JSON.stringify({ 'lat': data.coords.latitude, 'lng': data.coords.longitude })).subscribe(
            res => {
              let temp = res.val;
              res.val = parseInt(temp);
              this.nearest = res;
              console.log('new res', res);
              this.timeofStop = res;
  
              let stop_latLng = new google.maps.LatLng(res.wp_lat, res.wp_lng);
              let line = new google.maps.Polyline({
                path: [stop_latLng, latLng],
                geodesic: true,
                strokeColor: '#FF9900',
                strokeOpacity: 1.0,
                strokeWeight: 5.0,
                map: this.map
              });
              this.userLine.push(line);
  
  
              let marker = new google.maps.Marker({
                position: latLng,
                icon: image,
                map: this.map
              });
              this.userMarker.push(marker);
  
              this.map.setZoom(17);
              this.map.panTo(latLng);
  
  
            },
            err => {
              // Set the error information to display in the template
              this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
            }
          );
  
  
        });
  
  
      }
      else {
        this.userTracked = false;
        this.nearest = null;
        this.userMarker.map(marker => marker.setMap(null));
        this.userLine.map(line => line.setMap(null));
        let latLng = new google.maps.LatLng(16.7443885, 100.1945963);
        this.map.panTo(latLng);
        this.map.setZoom(15);
      }
      this.loading.dismiss();
  
    } */
    toggleOverlay(path) {
      let srcImage;
      let imageBounds = {
        north: 16.752252,
        south: 16.736286,
        east: 100.204607,
        west: 100.184093
      };
      let opt ={opacity :0.3}
      if (path == "RED") {
        srcImage = 'assets/RED.png';
      }
      else if (path == "YELLOW") {
        srcImage = 'assets/YELLOW.png';
      }
      if (this.historicalOverlay) {
        this.historicalOverlay.setMap(null);
      }
  
      this.historicalOverlay = new google.maps.GroundOverlay(
        srcImage,
        imageBounds,
        opt
      );
      this.historicalOverlay.setMap(this.map);
  
    }
  async loadMap() {
    this.loading = await this.loadingCtrl.create({
      message: 'Loading...'
    });
    await this.loading.present();
    let latLng = new google.maps.LatLng(16.7466885, 100.1945963);

    let mapOptions = {
      center: latLng,
      zoom: 15.5,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true

    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    /*               for (let i = 0; i <= this.data.length - 1; i++) {
            let marker: Marker = this.map.addMarkerSync({
              title: this.data[i].properties.name,
              position: { "lat": this.data[i].geometry.coordinates[1], "lng": this.data[i].geometry.coordinates[0] }
            });
          } */



    this.apiService.getBus_stop().subscribe(
      res => {
        console.log('RES', res.data);
        this.stop = res.data;

        for (let loc of this.stop) {

          let latLng = new google.maps.LatLng(loc.wp_lat, loc.wp_lng);
          let image = {
            url: 'assets/sign.png',
            // This marker is 20 pixels wide by 32 pixels high.
            scaledSize: new google.maps.Size(35, 35),
            //origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 36),
            
          };
          let stop_marker = new google.maps.Marker({
            icon: image,
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            title: loc.name,
            opacity:0.8,
            zIndex: 1

          });
          let info = new google.maps.InfoWindow({
            content: '<br>ป้าย : ' + loc.name + ' | สาย : '+loc.path +'</br>'+
                      loc.decs+'</p>'
          })
          stop_marker.addListener('click', () => {
            //this.stop_markers.map(stop_marker => stop_marker.setMap(null))
            this.map.setZoom(18),
              this.map.panTo(stop_marker.getPosition());
            info.open(this.map, stop_marker);
            this.timeofStop = loc;
            console.log('inTime', this.timeofStop)
          });
          info.addListener('closeclick', () => {
            this.timeofStop = null;
            console.log('CLICK')
            console.log('inTime', this.timeofStop)
          })
          this.stop_markers.push(stop_marker);


        }
        this.loading.dismiss();


      },
      err => {

        this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
      }
    );


  }

  animateCircle(line) {
    var count = 0;
    window.setInterval(function () {
      if (count < 200) {
        count = count + 2;
      }
      var icons = line.get('icons');
      icons[0].offset = (count / 2) + '%';
      line.set('icons', icons);

    }, 1);
  }


  updateMap(locations) {
    // Remove all current marker
    this.markers.map(marker => marker.setMap(null));
    this.lines.map(line => line.setMap(null));
    this.lines=[];
    this.markers = [];
    let RedlineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      scale: 8,
      strokeColor: '#C11'
    };

    let YellowlineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      scale: 8,
      strokeColor: '#EB0'
    };

    let EtclineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      scale: 8,
      strokeColor: '#3C3'
    };
    for (let loc of locations) {

      let latLng = new google.maps.LatLng(loc.wp_lat, loc.wp_lng);
      let lineSymbol: any;
      if (loc.bus_path == "RED") {
        lineSymbol = RedlineSymbol;

      }
      else if (loc.bus_path == "YELLOW") {
        lineSymbol = YellowlineSymbol;

      }
      else {
        lineSymbol = EtclineSymbol;
      }
      /*       let marker = new google.maps.Marker({
              map: this.map,
              animation: google.maps.Animation.DROP,
              position: latLng
            });
            this.map.panTo(latLng);
            this.markers.push(marker); */

      /*       if(loc.oldLoc == null){
              loc.oldLoc = {"lat :loc.lat, lng : loc.lng"};
            } */

      let line = new google.maps.Polyline({
        path: [{"lat":loc.oldLoc.lat,"lng":loc.oldLoc.lng}, latLng],
        icons: [{
          icon: lineSymbol,
          offset: '0%'
        }],
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 0.0,
        zIndex: 1000,
        map: this.map
      });
      
      //console.log(line)
      this.lines.push(line);
      console.log("LOC",loc);
      console.log("lines",this.lines.length);
      console.log("location",this.stream_datas);
      if(this.lines.length == this.stream_datas.length){

        this.animateCircle(line);
      }
      


/*       console.log('CHECK STOP0', this.timeofStop != null)
      if (this.timeofStop != null) {
        let body = {
          "path": loc.bus_path,
          "lat1": loc.wp_lat,
          "lng1": loc.wp_lng,
          "lat2": this.timeofStop.wp_lat,
          "lng2": this.timeofStop.wp_lng,
          "ave_velocity": loc.ave_velocity,
          "index": loc.oldLoc.index
        }
        console.log('body', body)
        this.apiService.calTime(body).subscribe(
          res => {
            console.log('TIME', res);
            let temp = res.time;
            let dist = parseInt(res.desc.dist);
            if (temp == null) {
              res.time = 'รอสักครู่'
            }
            else if (res.desc.dist == 0 && res.desc.path.length == 0) {
              res.time = 'รอบถัดไป'
            }
            else {
              res.time = parseInt(temp);
            }

            res.desc.dist = dist;
            loc.est = res
            console.log(loc);
            loc.oldLoc.lat = loc.wp_lat;
            loc.oldLoc.lng = loc.wp_lng;
            loc.oldLoc.index = loc.near_wp;
          },
          err => {

            this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
          }
        )

      } */

    }


  }
  receiveData(track) {
    if (track) {

      this.socket.connect()

      if (!this.socket.ioSocket.connected) {
        this.tracked = false;
        console.log('TRACK', this.tracked);
      }
      else {
        console.log('TRACK', this.tracked);
        this.socket.fromEvent('sendData').subscribe(data => {
          this.flow_data = data;
          console.log("Received", data)
          let checkBus = 0;

          if (this.stream_datas.length != 0) {
            console.log('this.flow_data.device_name ', this.flow_data.device_name);
            for (let i = 0; i <= this.stream_datas.length - 1; i++) {
              if (this.flow_data.device_name == this.stream_datas[i].device_name) {
                checkBus = 1;
                let oldLat = this.stream_datas[i].wp_lat;
                let oldLng = this.stream_datas[i].wp_lng;
                let oldIndex = this.stream_datas[i].near_wp;
                let v = parseFloat(this.stream_datas[i].ave_velocity);
                let v_km;

                if(v){
                  v_km = (v * 3.6).toFixed(2);
                }
                else{
                  v_km = 0;
                }
                this.stream_datas[i] = data;
                this.stream_datas[i].oldLoc = {
                  "lat": oldLat,
                  "lng": oldLng,
                  "index":oldIndex
                };
                this.stream_datas[i].v_km = v_km;
                if(this.stream_datas[i].bus_path == "RED"){
                  this.stream_datas[i].bus_path_name = "สีแดง";
                }
                else if(this.stream_datas[i].bus_path == "YELLOW"){
                  this.stream_datas[i].bus_path_name = "สีเหลือง";
                }

                break;
              }
              /*               else {
                              checkBus = 0;
                            } */
            }
          }
          if (checkBus == 0) {
            let v = parseFloat(this.flow_data.ave_velocity);
            let v_km;
            if(v){
              v_km = (v * 3.6).toFixed(2);
            }
            else{
              v_km = 0;
            }
            if(this.flow_data.bus_path == "RED"){
              this.flow_data.bus_path_name = "สีแดง";
            }
            else if(this.flow_data.bus_path == "YELLOW"){
              this.flow_data.bus_path_name = "สีเหลือง";
            }
            this.flow_data.v_km = v_km;
            this.flow_data.oldLoc = {
              "lat": this.flow_data.wp_lat,
              "lng": this.flow_data.wp_lng,
              "index": this.flow_data.oldIndex
            };
            this.stream_datas.push(this.flow_data)
          }
          console.log('list : ', this.stream_datas)
          console.log('CHECKSTOP--', this.timeofStop);
          console.log('CHECK STOP0', this.timeofStop != null)
          if (this.timeofStop != null) {
            let body = {
              "path": this.flow_data.bus_path,
              "lat1": this.flow_data.wp_lat,
              "lng1": this.flow_data.wp_lng,
              "lat2": this.timeofStop.wp_lat,
              "lng2": this.timeofStop.wp_lng,
              "ave_velocity": this.flow_data.ave_velocity,
              "index": this.flow_data.oldLoc.index
            }
            console.log('body', body)
            this.apiService.calTime(body).subscribe(
              res => {
                console.log('TIME', res);
                let temp = res.time;
                let dist = parseInt(res.desc.dist);
                if (temp == null) {
                  res.time = 'รอสักครู่'
                }
                else if (res.desc.dist == 0 && res.desc.path.length == 0) {
                  res.time = 'รอบถัดไป'
                }
                else {
                  res.time = parseInt(temp);
                }
    
                res.desc.dist = dist;
                this.flow_data.est = res
                console.log(this.flow_data);
                this.flow_data.oldLoc.lat = this.flow_data.wp_lat;
                this.flow_data.oldLoc.lng = this.flow_data.wp_lng;
                this.flow_data.oldLoc.index = this.flow_data.near_wp;
              },
              err => {
    
                this.error = `An error occurred, the data could not be retrieved: Status: ${err.status}, Message: ${err.statusText}`;
              }
            )
    
          }
          //this.updateMap(this.stream_datas);
          this.updateMap(this.stream_datas);
          if (this.flow_data.bus == this.following) {
            let flatLng = new google.maps.LatLng(this.flow_data.wp_lat, this.flow_data.wp_lng);
            this.map.panTo(flatLng);
          }


        })
      }
    }
    else {
      this.socket.disconnect();
    }
    console.log(track, this.tracked, this.socket.ioSocket.connected)
  }
  onClickFollow(e) {
    
    console.log(e);
    if (e.bus == this.following) {
      this.following = "";
    }
    else {
      this.following = e.bus;
    }
    if (e.bus_path == "RED") {
      this.createRedPath();
      this.toggleOverlay(e.bus_path);
    }
    else if (e.bus_path == "YELLOW") {
      this.createYellowPath();
      this.toggleOverlay(e.bus_path);
    }
    console.log('Follow : ', this.following);
    this.panToFollowing(e);
  }
  setIconColor(e) {
    if (e.bus_path == "RED") {
      
      return 'red-color';
    }
    else if (e.bus_path == "YELLOW") {
      return 'yellow-color';
    }
    else {
      return 'etc-color';
    }


  }
  getFollowClass(e) {
    if (e.bus == this.following) {
      
      return 'list-container-active';
    }
    else {
      return 'list-container';
    }
  }
  panToFollowing(e) {
    if (e.bus == this.following) {
      let latLng = new google.maps.LatLng(e.wp_lat, e.wp_lng);
      this.map.panTo(latLng);
      //this.map.setZoom(18);
      
    }
    else{
      this.historicalOverlay.setMap(null);
      console.log("CLEARRR",this.pathLine)
      this.pathLine.map(aline => aline.setMap(null));
      console.log("CLEARRR",this.pathLine)
    }

  }


  changeToggle() {
    location.reload();
  }


  async presentToast(msg) {

    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();


  }

  async presentAlertReconnect(msg) {
    const alert = await this.alertController.create({
      header: 'Connection Lost',
      message: 'Refresh or Reconnect again.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Reconnect',
          handler: () => {
            this.changeToggle();
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

}